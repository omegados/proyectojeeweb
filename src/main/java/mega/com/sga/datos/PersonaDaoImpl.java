package mega.com.sga.datos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.*;

import mega.com.sga.domain.Persona;

@Stateless
public class PersonaDaoImpl implements PersonaDao {
	
	@PersistenceContext(unitName="PersonaPU")
	EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> findAllPersonas() {
		
		return em.createNamedQuery("Persona.findAll").getResultList();   // Forma de consultar con NamedQueries (creadas en la clase Persona)
	}

	@Override
	public Persona findPersonaById(int idPersona) {
		return em.find(Persona.class, idPersona);    
	}

	@Override
	public Persona findPersonaByEmail(String email) {
		Query query = em.createQuery("from Persona p where p.email =: email");   // Forma de consultar sin NamedQueries
		query.setParameter("email", email);
		return (Persona) query.getSingleResult();
	}

	@Override
	public void insertPersona(Persona persona) {
		em.persist(persona);
	}

	@Override
	public void updatePersona(Persona persona) {
		em.merge(persona);
	}

	@Override
	public void deletePersona(Persona persona) {
		em.remove(em.merge(persona));
	}

}
