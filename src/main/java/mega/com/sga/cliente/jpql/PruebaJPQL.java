package mega.com.sga.cliente.jpql;

import java.util.Iterator;
import java.util.List;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mega.com.sga.domain.Persona;
import mega.com.sga.domain.Usuario;

public class PruebaJPQL {
	static Logger log = LogManager.getRootLogger();
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		String jpql = null;
		Query q = null;
		List<Persona> personas = null;
		List<Usuario> usuarios = null;
		Persona persona = null;
		Iterator iter = null;
		Object[] tupla = null;
		List<String> nombres = null;
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		
		// 1. Consulta de todos los objetos del tipo Persona
		log.debug("\n1. Consulta de todas las Personas");
		jpql = "select p from Persona p";
		personas = em.createQuery(jpql).getResultList();
//		mostrarPersonas(personas);
		
		// 2. Consulta de Persona con clave primaria
		log.debug("\n2. Consulta de la persona con Id = 1");
		jpql = "select p from Persona p where p.idPersona = 1";
		persona = (Persona) em.createQuery(jpql).getSingleResult();
//		log.debug(persona);

		// 3. Consulta de la Persona por nombre
		log.debug("\n3. Consulta de la persona con nombre = Manolo");
		jpql = "select p from Persona p where p.nombre = 'Manolo'";
		personas = em.createQuery(jpql).getResultList();
//		mostrarPersonas(personas);
		
		// 4. Consulta de datos individuales. Creamos un array de tipo Objecto de 3 columnas
		log.debug("\n4. Consutla de datos individuales");
		jpql = "select p.nombre as nombre, p.apellido as apellido, p.email as email from Persona p";
		iter = em.createQuery(jpql).getResultList().iterator();
		while(iter.hasNext()) {
			tupla = (Object[]) iter.next();
			String nombre = (String) tupla[0];
			String apellido = (String) tupla[1];
			String email = (String) tupla[2];
//			log.debug("Nombre: " + nombre + ", Apellido: " + apellido + ", Email: " + email);
		}
		
		// 5. Obtiene el objeto Persona y el Id. se crea un array de tipo Objecto de 2 columnas
		log.debug("\nObtenemos el objeto Persona y el Id con un array de 2 columnas");
		jpql = "select p, p.idPersona from Persona p";
		iter = em.createQuery(jpql).getResultList().iterator();
		while(iter.hasNext()) {
			tupla = (Object[]) iter.next();
			persona = (Persona) tupla[0];
			int idPersona = (int) tupla[1];
//			log.debug("Objeto Persona: " + persona );
//			log.debug("idPersona: " + idPersona);
		}
		
		// 6. Consulta de todas las personas
		log.debug("\nObtenemos un listado de las personas pero con sus atributos por defecto excepto el Id");
		jpql = "select new mega.com.sga.domain.Persona(p.idPersona) from Persona p";
		personas = em.createQuery(jpql).getResultList();
//		mostrarPersonas(personas);
		
		// 7. Regresa el valor mínimo y máximo del idPersona
		log.debug("\nRegresa el valor mínimo y máximo del idPersona");
		jpql = "select min(p.idPersona) as MinId, max(p.idPersona) as MaxId, count(p.idPersona) as Contador from Persona p";
		iter = em.createQuery(jpql).getResultList().iterator();
		while(iter.hasNext()) {
			tupla = (Object[]) iter.next();
			Integer idMin = (Integer) tupla[0];
			Integer idMax = (Integer) tupla[1];
			Long contador = (Long) tupla[2];
//			log.debug("idMin: " + idMin + " - idMax: " + idMax + " - Contador: " + contador);		
		}
		
		// 8. Cuenta los nombres diferentes que hay personas
		log.debug("\nCuenta los nombres diferentes que hay de personas");
		jpql = "select count(distinct p.nombre) from Persona p";
		Long count = (Long) em.createQuery(jpql).getSingleResult();
//		log.debug("Número de personas con nombres diferentes: " + count);
		
		// 9. Concatena y convierte a mayúsculas el nombre y apellido
		log.debug("\nConcatena y convierte a mayúsculas el nombre y apellido");
		jpql = "select concat(p.nombre, ' ', p.apellido) as Nombre from Persona p";
		nombres = em.createQuery(jpql).getResultList();
		for(String nombreCompleto: nombres) {
//			log.debug(nombreCompleto);
		}
		
		// 10. Obtiene el objeto persona con Id igual al parámetro proporcionado
		log.debug("\nObjeto Persona obtenido por el Id proporcionado por parámetro");
		int idPersona = 18;
		jpql = "select p from Persona p where p.idPersona = :id";
		q = em.createQuery(jpql);
		q.setParameter("id", idPersona);   // al parámetro :id de la consulta se le asigna la variable idPersona establecida como parámetro más arriba
		persona = (Persona) q.getSingleResult();
//		log.debug(persona);
		
		// 11. Obtiene las personas que contengan una letra 'a' en el nombre. No case-sensitive
		log.debug("\nObtiene las personas que contengan una letra 'a' en el nombre. No case-sensitive");
		String parametroString = "%a%";  // El porcentage es el símbolo que se usa como comodín en sql
		jpql = "select p from Persona as p where upper(p.nombre) like upper(:parametro)";
		q = em.createQuery(jpql);
		q.setParameter("parametro",  parametroString);
		personas = q.getResultList();
//		mostrarPersonas(personas);
		
		// 12. Uso de between
		log.debug("\nUso de between");
		jpql = "select p from Persona p where p.idPersona between 1 and 5";
		personas = em.createQuery(jpql).getResultList();
//		mostrarPersonas(personas);
		
		// 13. Uso del ordenamiento
		log.debug("\nUso del ordenamiento");
		jpql = "select p from Persona p where p.idPersona > 2 order by p.nombre desc, p.apellido desc";
		personas = em.createQuery(jpql).getResultList();
//		mostrarPersonas(personas);
		
		// 14. Uso de subquery
		log.debug("\nUso de subquery");
		jpql = "select p from Persona p where p.idPersona in (select min(p1.idPersona) from Persona p1)";
		personas = em.createQuery(jpql).getResultList();
//		mostrarPersonas(personas);
		
		// 15. Uso de join con lazy loading
		log.debug("\nUso de join con lazy loading");
		jpql = "select u from Usuario u join u.persona p";
		usuarios = em.createQuery(jpql).getResultList();
		mostrarUsuarios(usuarios);
		
		// 16. Uso de left join con eager loading
		log.debug("\nUso de left join con eager loading");
		jpql = "select u from Usuario u left join fetch u.persona";
		usuarios = em.createQuery(jpql).getResultList();
		mostrarUsuarios(usuarios);
		
	}
	
	private static void  mostrarPersonas(List<Persona> personas) {
		for (Persona p: personas) {
			log.debug(p);
		}
	}
	
	private static void  mostrarUsuarios(List<Usuario> usuarios) {
		for (Usuario u: usuarios) {
			log.debug(u);
		}
	}

}
