package mega.com.sga.cliente.asociaciones;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mega.com.sga.domain.Persona;
import mega.com.sga.domain.Usuario;

public class ClienteAsociacionesJPA {
	static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		
		@SuppressWarnings("unchecked")
		List<Persona> personas = em.createNamedQuery("Persona.findAll").getResultList();
		
		em.close();
		
		for(Persona persona: personas) {
			log.debug("Persona: " + persona);
			for (Usuario usuario: persona.getUsuarios()) {
				log.debug("Usuario: " + usuario);
			}
		}
	}
}
