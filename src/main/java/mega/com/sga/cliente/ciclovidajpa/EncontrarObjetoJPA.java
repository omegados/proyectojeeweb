package mega.com.sga.cliente.ciclovidajpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mega.com.sga.domain.Persona;

public class EncontrarObjetoJPA {
	static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		
		//Paso1. Inicio de transacción
		EntityTransaction tx = em.getTransaction();		
		tx.begin();
		
		//Paso 2. Ejecutamos SQL
		Persona persona1 = em.find(Persona.class,  1);

		
		//Paso 3 commit/rollback
		tx.commit();
		
		//Objeto en estado Detached
		log.debug("Objeto recuperado:" + persona1);
		
		//Cerramos en EntityManager
		em.close();
	}

}
