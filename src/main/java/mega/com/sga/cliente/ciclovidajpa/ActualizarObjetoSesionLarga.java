package mega.com.sga.cliente.ciclovidajpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mega.com.sga.domain.Persona;

public class ActualizarObjetoSesionLarga {
	static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		
		//Paso1. Inicio de transacción
		EntityTransaction tx = em.getTransaction();		
		tx.begin();
		
		//Paso 2. Ejecutamos SQL
		Persona persona1 = em.find(Persona.class,  1);
		log.debug("Objeto recuperado:" + persona1);

		//Paso 3 Modificamos nuestro objeto.
		persona1.setApellido("Rodríguez");
		
		//Paso 4 commit/rollback. Terminamos la transacción. Hemos recuperado y modificado el objeto en una sola transacción
		tx.commit();
		
		//Objeto en estado Detached
		log.debug("Objeto recuperado:" + persona1);
		
		//Cerramos en EntityManager
		em.close();
	}
}
