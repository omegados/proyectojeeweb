package mega.com.sga.cliente.ciclovidajpa;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mega.com.sga.domain.Persona;

public class PersistirObjetoJPA {
	static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		//Inicio de transacción
		//Paso1. Crea objeto. Objeto en estado transitivo
		Persona persona1 = new Persona("Mantin", "Martin@email.com", "Pedro", "958112233");
		// En este momento el objeto todavía no está ligado a la BD.
		
		//Paso 2. Inicio de transacción
		tx.begin();
		
		//Paso 3. Ejecutamos SQL
		em.persist(persona1);
		// Aquí todavía no se ha guardado en la BD y por tanto no tenemos la clave primaria aun
		log.debug("Objeto persistido - antes del commit (no PK):" + persona1);

		
		//Paso 4. commit/rollback
		tx.commit();
		
		//Objeto en estado Detached
		log.debug("Objeto persistido - estado detached:" + persona1);
		
		//Cerramos en EntityManager
		em.close();
	}

}
