package mega.com.sga.cliente.ciclovidajpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mega.com.sga.domain.Persona;

public class ActualizarObjetoJPA {
	static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		
		//Paso1. Inicio de transacción 1
		EntityTransaction tx = em.getTransaction();		
		tx.begin();
		
		//Paso 2. Ejecutamos SQL
		Persona persona1 = em.find(Persona.class,  1);

		
		//Paso 3 commit/rollback. Termina la transacción 1
		tx.commit();
		log.debug("Objeto recuperado:" + persona1);
		
		//Paso 4 modificamos nuestro objeto detached
		persona1.setApellido("Benalúa");
		
		//Paso 5 Inicio de transacción 2
		EntityTransaction tx2 = em.getTransaction();
		tx2.begin();
		
		//Paso 6 Actualizamos el campo de BD
		em.merge(persona1);
		
		//Paso 7 Termina la transacción 2
		tx2.commit();
				
		//Objeto en estado Detached
		log.debug("Objeto recuperado:" + persona1);
		
		//Cerramos en EntityManager
		em.close();
	}
}
