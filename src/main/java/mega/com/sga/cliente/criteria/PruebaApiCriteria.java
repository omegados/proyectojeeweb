package mega.com.sga.cliente.criteria;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import org.apache.logging.log4j.*;

import mega.com.sga.domain.Persona;

public class PruebaApiCriteria {
	static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		
		CriteriaBuilder cb = null;
		CriteriaQuery<Persona> criteriaQuery = null;
		Root<Persona> fromPersona = null;
		TypedQuery<Persona> query = null;
		Persona persona = null;
		List<Persona> personas = null;
		
		// Query utilizando el API de Criteria
		// 1. Consulta de todas las personas
		
		// El objeto EntityManager crea instancia CriteriaBuilder;
		cb = em.getCriteriaBuilder();
		
		// Se crea un objeto CriteriaQuery
		criteriaQuery = cb.createQuery(Persona.class);
		
		// Creamos el objeto raíz de query
		fromPersona = criteriaQuery.from(Persona.class);
		
		// Seleccionamos lo necesario del from
		criteriaQuery.select(fromPersona);
		
		// Creamos el query typeSafe
		query = em.createQuery(criteriaQuery);
		
		// Ejecutamos la consulta
		personas = query.getResultList();
		
//		mostrarPersonas(personas);
		
		// 2-a. Consulta de Persona por Id = 1 con Criteria
		//jpql = "select p from Persona p where p.idPersona = 1";
		
		log.debug("\n2-a. Consulta de la Prsona con id = 1");
		cb = em.getCriteriaBuilder();
		criteriaQuery = cb.createQuery(Persona.class);
		fromPersona = criteriaQuery.from(Persona.class);
		criteriaQuery.select(fromPersona).where(cb.equal(fromPersona.get("idPersona"), 1));
		persona = em.createQuery(criteriaQuery).getSingleResult();
		log.debug(persona);
		
		// 2-b. Consulta de la Persona con id =1
		log.debug("\n2-b. Consulta de la Persona con id = 1 con predicados");
		cb = em.getCriteriaBuilder();
		criteriaQuery = cb.createQuery(Persona.class);
		fromPersona = criteriaQuery.from(Persona.class);
		criteriaQuery.select(fromPersona);
		
		// La clase Predicate permite agregar varios criterios dinámicamente
		List<Predicate> criterios = new ArrayList<Predicate>();
		
		// Verificamos si tenemos criterios que agregar
		Integer idPersonaParam = 1;
		ParameterExpression<Integer> parameter = cb.parameter(Integer.class, "idPersona");
		criterios.add(cb.equal(fromPersona.get("idPersona"), parameter));
		
		// Se agregan los criterios
		if (criterios.isEmpty()) {
			throw new RuntimeException("Sin criterios");
		}
		else if(criterios.size() == 1) {
			criteriaQuery.where(criterios.get(0));
		}
		else {
			// Concatenamos todos los criterios que haya en el array de criterios mediante AND y se los pasamos al criteriaQuery
			criteriaQuery.where(cb.and(criterios.toArray(new Predicate[0])));
		}
		
		// Ya estamos listos para ejecutar el query
		query = em.createQuery(criteriaQuery);
		query.setParameter("idPersona", idPersonaParam);  // El valor creado dinámicamente en la variable idPersonaParam
		
		//Se ejecuta el query
		persona = query.getSingleResult();
		log.debug(persona);
		
		
		
	}
	
	private static void mostrarPersonas(List<Persona> personas) {
		for (Persona p: personas) {
			log.debug(p);
		}
	}
	

}
