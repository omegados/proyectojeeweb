package mega.com.sga.cliente.cascada;

import javax.persistence.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mega.com.sga.domain.Persona;
import mega.com.sga.domain.Usuario;

public class PersistenciaCascadaJPA {
	static Logger log = LogManager.getRootLogger();
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("SgaPU");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		
		tx.begin();
		
		Persona persona1 = new Persona("Hernández", "Hugo@mail.com", "Hugo", "958112233");
		
		Usuario usuario1 = new Usuario("123123", "Luis", persona1);
		
		// Si no tuviéramos configurada la persistencia en cascada primero deberíamos persistir el objeto "persona1" antes de hacer el del usuario
		// De esta manera al persistir al usaurio automáticamente se persiste "persona1" creándolo en la BD.
		em.persist(usuario1);
		
		tx.commit();
		
		log.debug("Objeto persistido persona1: " + persona1);
		log.debug("Objeto persistido usuario1: " + usuario1);
		
		em.close();
		
	}

}
