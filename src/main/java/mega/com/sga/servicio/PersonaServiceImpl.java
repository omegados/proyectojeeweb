package mega.com.sga.servicio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import mega.com.sga.datos.PersonaDao;
import mega.com.sga.domain.Persona;

@Stateless
public class PersonaServiceImpl implements PersonaServiceRemote, PersonaService{

	@Inject
	private PersonaDao personaDao;
	
	
	@Override
	public List<Persona> listarPersonas() {

		return personaDao.findAllPersonas();
	}

	@Override
	public Persona encontrarPersonaPorId(int idPersona) {
		return personaDao.findPersonaById(idPersona);
	}

	@Override
	public Persona encontrarPersonaPorEmail(String email) {
		return personaDao.findPersonaByEmail(email);
	}

	@Override
	public void registrarPersona(Persona persona) {
		personaDao.insertPersona(persona);
	}

	@Override
	public void modificarPersona(Persona persona) {
		personaDao.updatePersona(persona);
	}

	@Override
	public void eliminarPersona(Persona persona) {
		personaDao.deletePersona(persona);
	}

}
