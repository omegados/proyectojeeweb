package mega.com.sga.servicio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import mega.com.sga.datos.UsuarioDao;
import mega.com.sga.domain.Usuario;

@Stateless
public class UsuarioServiceImpl implements UsuarioService{

	@Inject
	private UsuarioDao usuarioDao;
	
	@Override
	public List<Usuario> listarUsuarios() {
		return usuarioDao.findAllUsuarios();
	}

	@Override
	public Usuario encontrarUsuarioPorId(int idUsuario) {
		return usuarioDao.findUsuarioById(idUsuario);
	}

	@Override
	public void registrarUsuario(Usuario usuario) {
		usuarioDao.insertUsuario(usuario);
	}

	@Override
	public void modificarUsuario(Usuario usuario) {
		usuarioDao.updateUsuario(usuario);
	}

	@Override
	public void eliminarUsuario(Usuario usuario) {
		usuarioDao.deleteUsuario(usuario);
	}

}
